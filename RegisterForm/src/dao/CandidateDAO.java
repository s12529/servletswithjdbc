package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import parsers.SQLCandidateParser;
import data.Candidate;

public class CandidateDAO {

	private final static String DBURL = "jdbc:mysql://bakemono.pl/servlet";
    private final static String DBUSER = "Santroy";
    private final static String DBPASS = "java";
    private final static String DBDRIVER = "com.mysql.jdbc.Driver";
    
    private Connection connection;
    private Statement statement;
    private String query;
    private SQLCandidateParser sqlCandidateParser;
    
    public CandidateDAO() {
        sqlCandidateParser = new SQLCandidateParser();
        try {
    		
    		Class.forName(DBDRIVER);
    		connection = DriverManager.getConnection(DBURL, DBUSER, DBPASS);
    		statement = connection.createStatement();
        } catch (SQLException | ClassNotFoundException e)
		{
        	e.printStackTrace();
		}
    }
    
    public void save(Candidate candidate) {
    	
    	query = sqlCandidateParser.createSaveQuery(candidate);
    	try {
			statement.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}  
    }
    
    public int count() {
    	
    	int count = 0;
    	
    	query = sqlCandidateParser.createGetCountQuery();

    		ResultSet rs;
			try {
				rs = statement.executeQuery(query);
			    while(rs.next()) {
    			count = rs.getInt(1);
			    }
			} catch (SQLException e) {
				e.printStackTrace();
			}

    	return count;
    }
    
    public List<Candidate> getCandidateList() {
    	
    	List<Candidate> candidates = new ArrayList<>();
    	Candidate candidate;
    	query = sqlCandidateParser.getAllCandidatesQuery();
    	
    	try {
    	ResultSet resultSet = statement.executeQuery(query);
    	
			while(resultSet.next()) {	
				candidate = new Candidate();
							
				candidate.setFirstName(resultSet.getString(2));
				candidate.setSurname(resultSet.getString(3));
				candidate.setEmail(resultSet.getString(4));
				candidate.setEmployer(resultSet.getString(5));
				candidate.setAdvert(resultSet.getString(6));
				candidate.setDescription(resultSet.getString(7));
				
				candidates.add(candidate);				
			}
		} 
    	catch (SQLException e) {
			e.printStackTrace();		
		}
    	
		return candidates;
    }	
}
