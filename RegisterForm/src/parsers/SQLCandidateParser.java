package parsers;

import data.Candidate;

public class SQLCandidateParser {
	
	private String query = "";

	public String createSaveQuery(Candidate candidate) {
		query = "INSERT INTO candidate VALUES (NULL, '"+candidate.getFirstName()+
				"', '"+candidate.getSurname()+"', '"+candidate.getEmail()+"', '"+candidate.getEmployer()+
				"', '"+candidate.getAdvert()+"', '"+candidate.getDescription()+"');";
        return query;
    }
	
	public String createGetCountQuery() {
		query = "SELECT COUNT(id) AS count FROM candidate;";
		return query;
	}
	
	public String getAllCandidatesQuery() {
		
		query = "SELECT * FROM candidate";
		return query;
		
	}
}
