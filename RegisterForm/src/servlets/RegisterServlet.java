package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CandidateDAO;
import parsers.CandidateParser;
import data.Candidate;

public class RegisterServlet extends HttpServlet {
	
	private static final long serialVersionUID = -8907720122415394771L;
	
	private CandidateParser candidateParser;
	private CandidateDAO candidateDao;
	private HttpSession session;
	
	public RegisterServlet() {
        candidateParser = new CandidateParser();
        candidateDao = new CandidateDAO();
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Candidate candidate = candidateParser.parseCandidateFromRequest(request);
		session = request.getSession();
		
		
			if(candidateDao.count() >= 5) {
				response.sendRedirect("nospace.jsp");
			} else {
				
				if(candidateParser.validateEmail(request)) {
				candidateDao.save(candidate);
				session.setAttribute("sessId", " ");
				response.sendRedirect("index.jsp");
				} else {
					String message = "<div class='alert alert-success alert-dismissable'><h4>Alert!</h4><strong>Warning!</strong> Confirm e-mail must be same as email. </div>";
					request.setAttribute("Message", message);
					request.getRequestDispatcher("registration.jsp").forward(request, response);
				}
			}
	}

}
