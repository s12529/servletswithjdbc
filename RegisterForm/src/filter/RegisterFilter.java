package filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CandidateDAO;

public class RegisterFilter implements Filter {

	private HttpSession session;
	private CandidateDAO candidateDao;
	
	public void destroy() {}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		session = req.getSession();	
		candidateDao = new CandidateDAO();
		
		if(req.getSession().getAttribute("sessId") != null) {		
			res.sendRedirect("alreadyregistered.jsp");
		} 
		
		if(candidateDao.count() >= 5) {
			res.sendRedirect("nospace.jsp");
			return;
		}
	
		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {}

}
