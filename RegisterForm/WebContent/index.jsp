<%@page import="dao.CandidateDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@include file = "bootstrap/bootstrap.html"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Java 4 US!</title>
</head>
<body>

<jsp:include page ="menu.jsp" />
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
		<div class="jumbotron">
				<h2>
					Witaj!
				</h2>
				<p>
					Jestes na stronie konferencyjnej "Java 4 US!". Zapraszamy do rejestracji.
				</p>
				<img src="bootstrap/unnamed.png"></img>
			</div>
		</div>
	</div>
</div>
<% 

CandidateDAO candidateDAO = new CandidateDAO();
int registeredCandidates = candidateDAO.count();

%>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			 <span class="label label-danger">Percent of already registered candidates</span>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
			<div class="col-md-12">
 				<div class="progress">
  					<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="1"  aria-valuemin="0" aria-valuemax="5" style="width:<% out.print(registeredCandidates*20); %>%">
    					<% out.print(registeredCandidates * 20); %>%
 					</div>
				</div>
			</div>
	</div>
</div>
    

</body>
</html>