<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@include file = "bootstrap/bootstrap.html"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Java 4 Us! - Registration Page</title>
</head>
<body>

<jsp:include page ="menu.jsp" />


<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
		<div class="jumbotron">
		${Message}
			<form role="form" action="RegisterServlet" method="POST">
			
			
				<div class="form-group"> 
					<label for="firstName">
						First Name
					</label>
					<input class="form-control" name="firstName" type="text" />
				</div>
				<div class="form-group">				 
					<label for="surname">
						Surname
					</label>
					<input class="form-control" name="surname" type="text" />
				</div>
				
				<div class="form-group">			 
					<label for="email">
						E-mail
					</label>
					<input class="form-control" name="email" type="email" />
				</div>
				
				<div class="form-group">	 
					<label for="repeat_email">
						Confirm e-mail
					</label>
					<input class="form-control" name="repeat_email" type="email" />
				</div>
				
			     <div class="form-group">	 
					<label for="employer">
						Employer
					</label>
					<input class="form-control" name="employer" type="text" />
				</div>
				
				 <div class="form-group">
  					<label for="ad">References</label>
  					<select class="form-control" name="ad">
  						<option value="Job">Job</option>
  						<option value="Study">Study</option>
  						<option value="Facebook">Facebook</option>
  						<option value="Friends">Friends</option>
  						<option value="Other">Other</option>
  				 </select>
				</div>
				
				 <div class="form-group">
 					 <label for="hobby">Comment</label>
 					 <textarea class="form-control" rows="5" name="hobby"></textarea>
				</div>	
				
				<button type="submit" class="btn btn-default">
					Submit
				</button>
			</form>
		</div>
		</div>
	</div>
</div>

</body>
</html>



