<%@page import="data.Candidate"%>
<%@page import="dao.CandidateDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@include file = "bootstrap/bootstrap.html"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Users List</title>
</head>
<body>

<jsp:include page ="menu.jsp" />
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="jumbotron">
					<div class="container">
  						<h2>Users List</h2>
  						<p>The list of users already registered for conference "Java 4 US!"</p>            
  						<table class="table">
    					<thead>
     						 <tr>
        						<th>Firstname</th>
        						<th>Lastname</th>
       							 <th>Email</th>
     						 </tr>
    					</thead>
   						 <tbody>
     						<%
								CandidateDAO candidateDAO = new CandidateDAO();

								for (Candidate candidate : candidateDAO.getCandidateList())
								{
									out.print("<tr class='active'><td>"+ candidate.getFirstName() +"</td><td>"+ candidate.getSurname() +"</td><td>"+ candidate.getEmail() +"</td></tr>");
								}
							%>
    					</tbody>
  					</table>
				</div>
			</div>
		</div>
	</div>
</div>

</body>
</html>