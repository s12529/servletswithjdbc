<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@include file = "bootstrap/bootstrap.html"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Already Registered</title>
</head>
<body>

<jsp:include page ="menu.jsp" />

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-success alert-dismissable">		
				<h4>
					Alert!
				</h4> <strong>Warning!</strong> Already registered. Thank you for your participation.
			</div>
		</div>
	</div>
</div>
</body>
</html>